﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorOnCollision : MonoBehaviour
{
    string colorId;
    private void Start()
    {
        Color randomColor = GetRandomColor();
        GetComponent<Renderer>().material.color = randomColor;
    }

    private Color GetRandomColor()
    {
        Color returnColor = new Color(1f, 1f, 1f);
        int color = Random.Range(0, 5);
        switch (color)
        {
            case 0: //RED
                returnColor = new Color(1f, 0f, 0f);
                gameObject.tag = "Red";
                break;
            case 1: //GREEN
                returnColor = new Color(0f, 0.7f, 0f);
                gameObject.tag = "Green";
                break;
            case 2: //BLUE
                returnColor = new Color(0f, 0f, 1f);
                gameObject.tag = "Blue";
                break;
            case 3: //BLACK
                returnColor = new Color(0f, 0f, 0f);
                gameObject.tag = "Black";
                break;
            case 4: //YELLOW
                returnColor = new Color(1f, 1f, 0f);
                gameObject.tag = "Yellow";
                break;
        }
        return returnColor;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Red") && gameObject.tag == "Red")
        {
            Destroy(gameObject);
            GameManager.Instance.score++;
        }
        if (other.gameObject.CompareTag("Green") && gameObject.tag == "Green")
        {
            Destroy(gameObject);
            GameManager.Instance.score++;
        }
        if (other.gameObject.CompareTag("Blue") && gameObject.tag == "Blue")
        {
            Destroy(gameObject);
            GameManager.Instance.score++;
        }
        if (other.gameObject.CompareTag("Yellow") && gameObject.tag == "Yellow")
        {
            Destroy(gameObject);
            GameManager.Instance.score++;
        }
        if (other.gameObject.CompareTag("White"))
        {
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("Black"))
        {
            Destroy(gameObject);
            GameManager.Instance.life--;
        }
       
    }


}
