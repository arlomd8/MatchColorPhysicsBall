﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorOnTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        //Color randomColor = GetRandomColorWithAlpha();
        //GetComponent<MeshRenderer>().material.color = randomColor;
    }

    private Color GetRandomColorWithAlpha()
    {
        return new Color(
            r: Random.Range(0f, 1f),
            g: Random.Range(0f, 1f),
            b: Random.Range(0f, 1f),
            a: 0.25f
            );
    }


}
