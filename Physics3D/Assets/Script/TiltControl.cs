﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltControl : MonoBehaviour
{
    [SerializeField] private float tiltSpeed = 5f;

    private void Update()
    {
        float Horizontal = -Input.GetAxis("Horizontal"); 
        float Vertical = -Input.GetAxis("Vertical");
        transform.Rotate(Vector3.back, Horizontal * Time.deltaTime * tiltSpeed); //a d
        transform.Rotate(Vector3.right, Vertical * Time.deltaTime * tiltSpeed); //w s

        if (transform.rotation.x >= 45f || transform.rotation.x <= -45f)
        {
            tiltSpeed = 0f;
        }
        if(transform.rotation.z >= 45f || transform.rotation.z <= -45f)
        {
            tiltSpeed = 0f;
        }
    }
}
