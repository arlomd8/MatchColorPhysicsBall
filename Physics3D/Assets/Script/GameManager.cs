﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public int score, life;
    public Text scoreText, timeText;
    public GameObject gameOver;


    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        life = 3;
        scoreText.text = "SCORE : " + score.ToString();
        timeText.text = "LIFE : " + life.ToString();
        gameOver.SetActive(false);
    }
    private void Update()
    {
        scoreText.text = "SCORE : " + score.ToString();
        timeText.text = "LIFE : " + life.ToString();

        if(life == 0)
        {
            gameOver.SetActive(true);
            Time.timeScale = 0;
        }
    }
}

