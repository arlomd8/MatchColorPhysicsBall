﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emitter : MonoBehaviour
{
    public GameObject ball;
    public Transform spawnPos;
    public float spawnCooldown = 1;
    private float timeUntilSpawn = 0;

    void Update()
    {
        timeUntilSpawn -= Time.deltaTime;
        if (timeUntilSpawn <= 0)
        {
            GameObject octo = Instantiate(ball, spawnPos.position, Quaternion.identity) as GameObject;
            timeUntilSpawn = spawnCooldown;
        }
    }
}
